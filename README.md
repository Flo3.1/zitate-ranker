# Zitate Ranker
This Project aims to create an App whit which you can rank quotes and store the Result on a central Database.

## The Server
The Server is written in python with the Http.server library and has no security mesures against DoS attacks. The admin tools are only accesible with an ssh connection to the server.

The default database is a PostgreSQL database running in Docker.


## The client 
The client is written in C++ with the QT libraries and designed for Android.

