#include "funtions.h"


QString jsonToMDN(nlohmann::json quote)
{
	QString data="";
	data+="## ";
	data+=escapeMDN(quote["title"].get<std::string>().c_str());
	data+="\n";

	for(int i=0; i<quote["quote"]["lenght"].get<int>(); i++){
		if(quote["quote"]["data"][QString::number(i).toStdString().c_str()]["datatype"].get<std::string>()=="P"){

			data+="*";
			data+=escapeMDN(quote["quote"]["data"][QString::number(i).toStdString().c_str()]["happening"].get<std::string>().c_str());
			data+="*\\\n";


		}else{
			data+="\"";
			data+=escapeMDN(quote["quote"]["data"][QString::number(i).toStdString().c_str()]["quote"].get<std::string>().c_str());
			data+="\"\\\n";

			data+="-";
			data+=escapeMDN(quote["quote"]["data"][QString::number(i).toStdString().c_str()]["person"].get<std::string>().c_str());
			data+="\\\n";
		}
	}

	/*
	for(int i=0; i<quote["quote"]["lenght"].get<int>(); i++){
		if(quote["quote"]["data"][QString::number(i).toStdString().c_str()]["datatype"].get<std::string>()!="P"){

		}
	}*/

	data+=escapeMDN(quote["date"].get<std::string>().c_str());

	return data;
}

QString escapeMDN(QString inp)
{

	for(int i=0; i<inp.length(); i++){
		if(QString(MDN_SET).contains(inp[i])){
			inp.insert(i,'\\');
			i++;

		}


	}

	return inp;
}
