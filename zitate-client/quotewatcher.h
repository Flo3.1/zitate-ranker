#ifndef QUOTEWATCHER_H
#define QUOTEWATCHER_H

#include <QWidget>

namespace Ui {
class quoteWatcher;
}

class quoteWatcher : public QWidget
{
	Q_OBJECT

public:
	explicit quoteWatcher(QString* srvr, int id, QWidget *parent = nullptr);
	~quoteWatcher();
	QString *server;
private:
	Ui::quoteWatcher *ui;
};

#endif // QUOTEWATCHER_H
