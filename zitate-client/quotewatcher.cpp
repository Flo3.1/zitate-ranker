#include "quotewatcher.h"
#include "ui_quotewatcher.h"

#include "HTTPRequest/include/HTTPRequest.hpp"
#include "nlohmann/json.hpp"
#include "funtions.h"

quoteWatcher::quoteWatcher(QString* srvr, int id, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::quoteWatcher)
{
	server=srvr;
	ui->setupUi(this);


	http::Request getquote1{(QString("http://")+*server+QString("/api/quote/byid/") + QString::number(id)).toStdString().c_str()};
	http::Response response = getquote1.send("GET");
	std::string res=std::string{response.body.begin(), response.body.end()};

	nlohmann::json quote = nlohmann::json::parse(res);
	ui->quote->setMarkdown(jsonToMDN(quote));
	ui->points->setText(QString("Punkte: ")+QString::number(quote["points"].get<float>()));
	ui->votes->setText(QString("Votes: ")+QString::number(quote["votes"].get<int>()));


}

quoteWatcher::~quoteWatcher()
{
	delete ui;
}
