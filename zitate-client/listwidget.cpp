#include "listwidget.h"
#include "ui_listwidget.h"
#include "HTTPRequest/include/HTTPRequest.hpp"
#include "nlohmann/json.hpp"
#include <iostream>

void listWidget::fetchQuotes()
{
	deleteQuotes();
	//http request
	try
	{
		QString url=QString("http://")+*server+QString("/api/quote/list/");
		url+=sortIndex[ui->sort_key->currentIndex()];
		url+="/";
		url+=up_downIndex[ui->up_down->currentIndex()];
		url+="/";
		url+=QString::number(ui->pageBox->currentIndex());



		http::Request getrandos{url.toStdString().c_str()};
		http::Response response = getrandos.send("GET");

		std::string res=std::string{response.body.begin(), response.body.end()};
		nlohmann::json jdata = nlohmann::json::parse(res);
		int l=jdata["lenght"].get<int>();
		l=l>=QUOTES_FETCHED?QUOTES_FETCHED:l;

		for(int i=0; i<l; i++){
			//QString ptitle=
			datas[i]=new qouteItem(
						jdata[QString::number(i).toStdString().c_str()]["id"].get<int>(),
						jdata[QString::number(i).toStdString().c_str()]["title"].get<std::string>().c_str(),
						jdata[QString::number(i).toStdString().c_str()]["date"].get<std::string>().c_str(),
						jdata[QString::number(i).toStdString().c_str()]["points"].get<float>());

			ui->gridLayout->addWidget(datas[i]->id, i+2, 0);
			ui->gridLayout->addWidget(datas[i]->title, i+2, 1);
			ui->gridLayout->addWidget(datas[i]->date, i+2, 2);
			ui->gridLayout->addWidget(datas[i]->points, i+2, 3);
			ui->gridLayout->addWidget(datas[i]->go, i+2, 4);
			connect(datas[i], SIGNAL(selected(int)), this, SLOT(quoteSelect	(int)));
			//((QVBoxLayout*)layout())->addLayout(datas[i]);

		}


	}
	catch (const std::exception& e)
	{
		msg.setIcon(QMessageBox::Critical);
		msg.setText(e.what());
		msg.exec();
		std::cerr << "Request failed, error: " << e.what() << '\n';
	}


}

void listWidget::deleteQuotes()
{
	for(int i=0; i<QUOTES_FETCHED; i++){
		if(datas[i]!=NULL){
			delete datas[i];
			datas[i]=NULL;
		}


	}
}

listWidget::listWidget(QApplication* iapp, QString * srvadr, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::listWidget)
{
	server=srvadr;
	for(int i=0; i<QUOTES_FETCHED; i++){
		datas[i]=NULL;
	}
	ui->setupUi(this);
	for(int i=0; i<PAGES; i++){
		ui->pageBox->addItem(QString::number(i+1));

	}
	connect(ui->goButton, SIGNAL(pressed()), this, SLOT(goPressed()));
	fetchQuotes();


}

listWidget::~listWidget()
{
	deleteQuotes();
	//delete vbLayout;
	delete ui;
}

void listWidget::goPressed()
{
	fetchQuotes();
}

void listWidget::quoteSelect(int id)
{
	emit quoteSelected(id);
}
