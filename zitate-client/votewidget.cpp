#include "votewidget.h"
#include "ui_votewidget.h"
#include <iostream>
#include <QDebug>
#include "funtions.h"
#include "HTTPRequest/include/HTTPRequest.hpp"
#include "nlohmann/json.hpp"


VoteWidget::VoteWidget(QApplication* iapp, QString *svradr, QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::VoteWidget)
{
	app=iapp;
	ui->setupUi(this);
	serverAddress=svradr;
	newRequests();
	connect(ui->skip, SIGNAL(pressed()), this, SLOT(newquote()));
	connect(ui->voteSubmit, SIGNAL(pressed()), this, SLOT(sendVote()));

	// These are for Desktop
	connect(ui->voteQuote1, SIGNAL(released()), this, SLOT(uppervote()));
	connect(ui->voteQuote2, SIGNAL(released()), this, SLOT(lowervote()));
	connect(ui->voteEqual, SIGNAL(released()), this, SLOT(equalvote()));

	// And these for Android (for some reason this is nececary, because released doesn't exist on toch screens)
	connect(ui->voteQuote1, SIGNAL(pressed()), this, SLOT(uppervote()));
	connect(ui->voteQuote2, SIGNAL(pressed()), this, SLOT(lowervote()));
	connect(ui->voteEqual, SIGNAL(pressed()), this, SLOT(equalvote()));


	connect(ui->report_1, SIGNAL(pressed()), this, SLOT(report_upper()));
	connect(ui->report_2, SIGNAL(pressed()), this, SLOT(report_lower()));


	ui->voteQuote1->setCheckable(true);
	ui->voteQuote2->setCheckable(true);
	ui->voteEqual->setCheckable(true);


}

VoteWidget::~VoteWidget()
{
	delete ui;
}

void VoteWidget::newRequests()
{
	try
	{
		choice=nothing;
		http::Request getrandos{(QString("http://")+*serverAddress+QString("/api/quote/rndpair")).toStdString().c_str()};
		http::Response response = getrandos.send("GET");

		std::string res=std::string{response.body.begin(), response.body.end()};
		nlohmann::json jdata = nlohmann::json::parse(res);


		id1=jdata["0"].get<int>();
		id2=jdata["1"].get<int>();



		http::Request getquote1{(QString("http://")+*serverAddress+QString("/api/quote/byid/") + QString::number(id1)).toStdString().c_str()};
		response = getquote1.send("GET");
		res=std::string{response.body.begin(), response.body.end()};

		nlohmann::json quote1 = nlohmann::json::parse(res);
		ui->quoteField1->setMarkdown(jsonToMDN(quote1));


		http::Request getquote2{(QString("http://")+*serverAddress+QString("/api/quote/byid/") + QString::number(id2)).toStdString().c_str()};
		response = getquote2.send("GET");
		res=std::string{response.body.begin(), response.body.end()};

		nlohmann::json quote2 = nlohmann::json::parse(res);
		ui->quoteField2->setMarkdown(jsonToMDN(quote2));

		setButtons();


	}
	catch (const std::exception& e)
	{
		msg.setIcon(QMessageBox::Critical);
		msg.setText(e.what());
		msg.exec();
		std::cerr << "Request failed, error: " << e.what() << '\n';
	}

}

void VoteWidget::setButtons()
{

	ui->voteSubmit->setEnabled(true);
	ui->voteQuote1->setChecked(false);
	ui->voteQuote2->setChecked(false);
	ui->voteEqual->setChecked(false);

	switch (choice) {
	case nothing:
		ui->voteSubmit->setEnabled(false);
		break;
	case quote1:
		ui->voteQuote1->setChecked(true);
		break;
	case quote2:
		ui->voteQuote2->setChecked(true);
		break;
	case equal:
		ui->voteEqual->setChecked(true);
		break;
	}



}

void VoteWidget::newquote()
{
	newRequests();
}

void VoteWidget::uppervote()
{
	choice=quote1;
	setButtons();
}
void VoteWidget::lowervote()
{
	choice=quote2;
	setButtons();
}
void VoteWidget::equalvote()
{
	choice=equal;
	setButtons();
}

void VoteWidget::report_upper()
{
	emit report(id1);
}

void VoteWidget::report_lower()
{
	emit report(id2);
}

void VoteWidget::sendVote()
{
	try
	{
		QString url="";
		switch (choice) {
			case equal:
				url=QString("http://")+*serverAddress+QString("/api/quote/equal/")+QString::number(id1)+"/"+QString::number(id2);
			break;
			case quote1:
				url=QString("http://")+*serverAddress+QString("/api/quote/rate/")+QString::number(id1)+"/"+QString::number(id2);
			break;
			case quote2:
				url=QString("http://")+*serverAddress+QString("/api/quote/rate/")+QString::number(id2)+"/"+QString::number(id1);
			break;
			default:
				break;
		}

		http::Request voter{url.toStdString().c_str()};
		http::Response response = voter.send("POST");


		newRequests();



	}
	catch (const std::exception& e)
	{
		msg.setIcon(QMessageBox::Critical);
		msg.setText(e.what());
		msg.exec();
		std::cerr << "Request failed, error: " << e.what() << '\n';
	}
}

