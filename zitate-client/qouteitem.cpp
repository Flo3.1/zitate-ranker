#include "qouteitem.h"

qouteItem::qouteItem(int ids, QString titles, QString dates, float pointss)
{
	intid=ids;
	id=new QLabel(QString::number(ids));
	title=new QLabel(titles);
	date=new QLabel(dates);
	points=new QLabel(QString::number(pointss, 'f', 0));
	go=new QPushButton("→");
	connect(go, SIGNAL(pressed()), this, SLOT(pressed()));

	if(title->text().length()>=20){
		QString str;
		for(int i=0; i<20; i++)
			str+=titles[i];
		title->setText(QString(str)+"...");

	}


	title->sizePolicy().setVerticalPolicy(QSizePolicy::Expanding);
	//title->sizeHint().setWidth(1000);

	id->sizePolicy().setHorizontalPolicy(QSizePolicy::Fixed);
	id->sizeHint().setWidth(5);


	title->sizePolicy().setHorizontalPolicy(QSizePolicy::Fixed);
	title->sizeHint().setWidth(300);

	date->sizePolicy().setHorizontalPolicy(QSizePolicy::Fixed);
	date->sizeHint().setWidth(5);

	points->sizePolicy().setHorizontalPolicy(QSizePolicy::Fixed);
	points->sizeHint().setWidth(5);


	go->sizePolicy().setHorizontalPolicy(QSizePolicy::Fixed);
	go->sizeHint().setWidth(1);


	addWidget(id);
	addWidget(title);
	addWidget(date);
	addWidget(points);
	addWidget(go);
}

qouteItem::~qouteItem()
{
	delete id;
	delete title;
	delete date;
	delete points;
	delete go;
}

void qouteItem::pressed()
{
	emit selected(intid);
}


