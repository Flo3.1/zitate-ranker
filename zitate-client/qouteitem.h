#ifndef QOUTEITEM_H
#define QOUTEITEM_H

#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>




class qouteItem : public QHBoxLayout
{
	Q_OBJECT
public:
	int intid;
	QLabel *id;
	QLabel *title;
	QLabel *points;
	QLabel *date;
	QPushButton *go;


	qouteItem(int ids, QString titles, QString dates, float pointss);
	~qouteItem();
public slots:
	void pressed();


signals:
	void selected(int id);

};

#endif // QOUTEITEM_H
