#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include "qouteitem.h"
#include <QAction>
#include <QMessageBox>

const QString sortIndex[6]={"id", "datum","reports", "points", "title", "RANDOM()"};
const QString up_downIndex[2]={"DESC", "ASC"};



#define QUOTES_FETCHED 20
#define PAGES 20

namespace Ui {
class listWidget;
}

class listWidget : public QWidget
{
	Q_OBJECT

public:
	QMessageBox msg;
	void fetchQuotes();
	void deleteQuotes();
	qouteItem* datas[QUOTES_FETCHED];
	//QVBoxLayout *vbLayout;
	QString * server;
	explicit listWidget(QApplication* iapp, QString *srvadr, QWidget *parent = nullptr);
	~listWidget();

private:
	Ui::listWidget *ui;

public slots:
	void goPressed();
	void quoteSelect(int id);

signals:
	void quoteSelected(int id);



};

#endif // LISTWIDGET_H
