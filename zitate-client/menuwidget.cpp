#include "menuwidget.h"
#include "ui_menuwidget.h"

MenuWidget::MenuWidget(QApplication* iapp, QString* svradr, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MenuWidget)
{
	ui->setupUi(this);
	setFocus();
	server=svradr;
	adressline.setText(*server);
	adressline.setPlaceholderText("domain:port");
	connect(ui->btnVote, SIGNAL(pressed()), this, SLOT(votingPressed()));
	connect(ui->btnTop, SIGNAL(pressed()), this, SLOT(listPressed()));
	connect(&adressline, SIGNAL(requestKeyboard()), this, SLOT(requestKeyboard()));
	connect(&adressline, SIGNAL(editingFinished()), this, SLOT(addresDone()));
	ui->horizontalLayout_top->insertWidget(1,&adressline);
	app=iapp;
}

MenuWidget::~MenuWidget()
{
	delete ui;
}

void MenuWidget::requestKeyboard()
{
	app->inputMethod()->show();
}

void MenuWidget::votingPressed()
{
	emit openVoting();
}

void MenuWidget::listPressed()
{
	emit openList();
}

void MenuWidget::addresDone()
{
	setFocus();
	*server=adressline.text();
}


