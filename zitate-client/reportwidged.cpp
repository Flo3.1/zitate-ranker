#include "reportwidged.h"
#include "ui_reportwidged.h"
#include "HTTPRequest/include/HTTPRequest.hpp"
#include "nlohmann/json.hpp"
#include "funtions.h"

reportwidged::reportwidged(QApplication* iapp, QString* srvr, int id, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::reportwidged)
{
	ui->setupUi(this);
	app=iapp;
	server=srvr;
	intid=id;
	nameline.setPlaceholderText("Name");
	contentline.setPlaceholderText("Reason");


	http::Request getquote1{(QString("http://")+*server+QString("/api/quote/byid/") + QString::number(id)).toStdString().c_str()};
	http::Response response = getquote1.send("GET");
	std::string res=std::string{response.body.begin(), response.body.end()};

	nlohmann::json quote = nlohmann::json::parse(res);
	ui->quote->setMarkdown(jsonToMDN(quote));



	ui->verticalLayout->insertWidget(1, &nameline);
	connect(&nameline, SIGNAL(requestKeyboard()), this, SLOT(requestKeyboard()));
	connect(&nameline, SIGNAL(editingFinished()), this, SLOT(editFinished()));

	ui->verticalLayout->insertWidget(1, &contentline);
	connect(&contentline, SIGNAL(requestKeyboard()), this, SLOT(requestKeyboard()));
	connect(&contentline, SIGNAL(editingFinished()), this, SLOT(editFinished()));


	connect(ui->buttonSubmitt, SIGNAL(pressed()), this, SLOT(sendRequest()));


}

reportwidged::~reportwidged()
{
	delete ui;
}

void reportwidged::requestKeyboard()
{
	app->inputMethod()->show();
}

void reportwidged::editFinished()
{
	setFocus();
	name=nameline.text();
	comment=contentline.text();
}

void reportwidged::sendRequest()
{

	nlohmann::json quote;
	quote["message"]=comment.toStdString();
	quote["name"]=name.toStdString();

	http::Request psotreport{(QString("http://")+*server+QString("/api/quote/report/") + QString::number(intid)).toStdString().c_str()};
	qDebug()<<quote.dump().c_str();
	psotreport.send("POST", quote.dump());

	qDebug()<<"done";

	emit back();
}
