QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    flineedit.cpp \
    funtions.cpp \
    listwidget.cpp \
    main.cpp \
    mainwindow.cpp \
    menuwidget.cpp \
    qouteitem.cpp \
    quotewatcher.cpp \
    reportwidged.cpp \
    votewidget.cpp

HEADERS += \
    HTTPRequest/include/HTTPRequest.hpp \
    defines.h \
    flineedit.h \
    funtions.h \
    listwidget.h \
    mainwindow.h \
    menuwidget.h \
    nlohmann/json.hpp \
    qouteitem.h \
    quotewatcher.h \
    reportwidged.h \
    votewidget.h

FORMS += \
    listwidget.ui \
    menuwidget.ui \
    quotewatcher.ui \
    reportwidged.ui \
    votewidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}
