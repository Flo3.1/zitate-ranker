#ifndef REPORTWIDGED_H
#define REPORTWIDGED_H

#include <QWidget>
#include "flineedit.h"



namespace Ui {
class reportwidged;
}

class reportwidged : public QWidget
{
	Q_OBJECT

public:
	explicit reportwidged(QApplication* iapp, QString* srvr, int id, QWidget *parent = nullptr);
	~reportwidged();
	fLineEdit nameline;
	fLineEdit contentline;

private:
	int intid;
	QString * server;
	Ui::reportwidged *ui;
	QApplication* app;
	QString name;
	QString comment;


signals:
	void back();


public slots:
	void sendRequest();
	void requestKeyboard();
	void editFinished();
};

#endif // REPORTWIDGED_H
