#include "mainwindow.h"
#include "nlohmann/json.hpp"

void MainWindow::changewidget()
{

	cleanMemory();

	switch (currentmode) {
		case mainmenu:
			menu= new MenuWidget(app, &server);
			connect(menu, SIGNAL(openVoting()), this, SLOT(openVoting()));
			connect(menu, SIGNAL(openList()), this, SLOT(openList()));
			setCentralWidget(menu);
		break;


		case voting:
			voter= new VoteWidget(app, &server);
			connect(voter, SIGNAL(report(int)), this, SLOT(reportQuote(int)));
			setCentralWidget(voter);
		break;


		case listmode:
			list= new listWidget(app, &server);
			connect(list, SIGNAL(quoteSelected(int)), this, SLOT(openQuote(int)));
			setCentralWidget(list);
		break;
		case quotemode:
			qwidget=new quoteWatcher(&server, quoteID);
			setCentralWidget(qwidget);
		break;
		case reportfromvote:
			rep=new reportwidged(app, &server, quoteID);
			connect(rep, SIGNAL(back()), this, SLOT(rBack()));
			setCentralWidget(rep);
		break;

	}
}

void MainWindow::cleanMemory()
{

	if(voter!=NULL){
		delete voter;
		voter=NULL;
	}
	if(menu!=NULL){
		delete menu;
		menu=NULL;
	}
	if(qwidget!=NULL){
		delete qwidget;
		qwidget=NULL;
	}
	if(list!=NULL){
		delete qwidget;
		qwidget=NULL;
	}
	if(rep!=NULL){
		delete rep;
		rep=NULL;
	}


}

MainWindow::MainWindow(QApplication* iapp, QWidget *parent) : QMainWindow(parent)
{
	save=new QFile(SAVE_FILE_LOCATION);

	if(save->exists()){
		save->open(QIODeviceBase::ReadOnly | QIODeviceBase::Text);
		QString data=save->readAll();

		try
		{
			try
			{
				config = nlohmann::json::parse(data.toStdString().c_str());
				server=config.at("url").get<std::string>().c_str();
			}
			catch (nlohmann::json::type_error& e)
			{
				qDebug()<< e.what() << '\n';
			}
		}
		catch (nlohmann::json::parse_error& e)
		{
			qDebug()<<e.what()<<'\n';
		}
		save->close();
	}else{
		qDebug()<<SAVE_FILE_LOCATION<<" doesn't exist\n"<<save->errorString().toStdString().c_str()<<'\n';
	}
	app=iapp;
	currentmode=mainmenu;
	changewidget();
}

MainWindow::~MainWindow()
{
	if(!save->exists(SAVE_FILE_LOCATION)){
		qDebug()<<SAVE_FILE_LOCATION<<" doesn't exist\n"<<save->errorString().toStdString().c_str()<<'\n';
	}
	save->open(QIODeviceBase::WriteOnly);
	config["url"]=server.toStdString().c_str();
	save->write(QString(config.dump().c_str()).toUtf8());
	save->close();
	delete save;
	cleanMemory();
}

void MainWindow::openVoting()
{
	currentmode=voting;
	changewidget();
}

void MainWindow::openMainmenu()
{

	currentmode=mainmenu;
	changewidget();
}

void MainWindow::openList()
{
	currentmode=listmode;
	changewidget();
}

void MainWindow::openQuote(int id)
{
	quoteID=id;
	currentmode=quotemode;
	changewidget();
}

void MainWindow::reportQuote(int id)
{
	quoteID=id;
	currentmode=reportfromvote;
	changewidget();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Back){
		requestBack();
	}else if(event->key() == Qt::Key_Escape){
		requestBack();
	}else{
		qDebug()<<event->key();
	}
	event->accept();
}

void MainWindow::requestBack()
{
	switch (currentmode) {
		case mainmenu:
			app->exit(0);
		break;
		case voting:
			currentmode=mainmenu;
		break;
		case listmode:
			currentmode=mainmenu;
		break;
		case quotemode:
			currentmode=listmode;
		break;
		case reportfromvote:
			currentmode=voting;
		break;

	}

	changewidget();
}

void MainWindow::rBack()
{
	requestBack();
}
