#ifndef VOTEWIDGET_H
#define VOTEWIDGET_H

#include <QWidget>
#include <QMessageBox>


enum ballot{nothing, quote1, quote2, equal};

QT_BEGIN_NAMESPACE
namespace Ui { class VoteWidget; }
QT_END_NAMESPACE

class VoteWidget : public QWidget
{
	Q_OBJECT

public:
	VoteWidget(QApplication* iapp, QString *svradr, QWidget *parent = nullptr);
	~VoteWidget();
	void newRequests();
	QString *serverAddress;
private:
	QMessageBox msg;
	QApplication* app;
	ballot choice;
	int id1, id2;
	Ui::VoteWidget *ui;

	void setButtons();

signals:


	void backToMenu();
	void report(int id);

public slots:
	void newquote();

	void uppervote();
	void lowervote();
	void equalvote();
	void report_upper();
	void report_lower();


	void sendVote();


};
#endif // VOTEWIDGET_H
