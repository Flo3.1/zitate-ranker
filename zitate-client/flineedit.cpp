#include "flineedit.h"

fLineEdit::fLineEdit()
{
	sizePolicy().setHorizontalPolicy(QSizePolicy::MinimumExpanding);
}

void fLineEdit::focusInEvent(QFocusEvent *e)
{
	emit requestKeyboard();
}
