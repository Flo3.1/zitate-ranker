#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "votewidget.h"
#include "menuwidget.h"
#include "listwidget.h"
#include "quotewatcher.h"
#include "reportwidged.h"

#include <QKeyEvent>
#include <QApplication>
#include "defines.h"
#include <QFile>
#include "nlohmann/json.hpp"

enum mode{mainmenu, voting, listmode, quotemode, reportfromvote};

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	mode currentmode;
	void changewidget();
	void cleanMemory();
	VoteWidget *voter=NULL;
	MenuWidget *menu=NULL;
	listWidget *list=NULL;
	reportwidged *rep=NULL;
	quoteWatcher * qwidget=NULL;
	QApplication* app;
	QFile* save;
	nlohmann::json config;
	QString server=DEFAULT_SOCKET;
	int quoteID;
	explicit MainWindow(QApplication* iapp, QWidget *parent = nullptr);
	~MainWindow();
	void keyPressEvent(QKeyEvent *event);
	void requestBack();
signals:

public slots:
	void rBack();
	void openVoting();
	void openMainmenu();
	void openList();
	void openQuote(int id);
	void reportQuote(int id);

};

#endif // MAINWINDOW_H
