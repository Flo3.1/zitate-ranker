#ifndef FLINEEDIT_H
#define FLINEEDIT_H

#include <QLineEdit>



class fLineEdit : public QLineEdit
{
	Q_OBJECT
public:
	fLineEdit();
	void focusInEvent(QFocusEvent *e);

signals:
	void requestKeyboard();
};

#endif // FLINEEDIT_H
