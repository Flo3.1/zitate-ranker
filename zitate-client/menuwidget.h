#ifndef MENUWIDGET_H
#define MENUWIDGET_H

#include <QWidget>
#include "flineedit.h"

namespace Ui {
class MenuWidget;
}

class MenuWidget : public QWidget
{
	Q_OBJECT

public:
	explicit MenuWidget(QApplication* iapp, QString* svradr, QWidget *parent = nullptr);
	~MenuWidget();
	QApplication* app;
private:
	QString * server;
	Ui::MenuWidget *ui;
	fLineEdit adressline;


signals:
	void openVoting();
	void openList();

public slots:
	void requestKeyboard();
	void votingPressed();
	void listPressed();
	void addresDone();


};

#endif // MENUWIDGET_H
