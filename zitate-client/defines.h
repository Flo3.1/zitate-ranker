#ifndef DEFINES_H
#define DEFINES_H

#include <QStandardPaths>
#include <QDir>

#ifdef ANDROID

#define SAVE_FILE_LOCATION "/data/user/0/at.fplasun.quoter/config.json"

#elif linux

#define SAVE_FILE_LOCATION (QDir::homePath() +"/.config/quoter/config.json")

#else

#define SAVE_FILE_LOCATION "config.json"

#endif

#define DEFAULT_SOCKET "localhost:8000"
#define CONFIG_URL_NAME "url"

#endif // DEFINES_H
