#ifndef FUNTIONS_H
#define FUNTIONS_H

#include <QString>
#include "nlohmann/json.hpp"

#define MDN_SET "*_~<>[]()\\\"'"

QString jsonToMDN(nlohmann::json quote);
QString escapeMDN(QString inp);


#endif // FUNTIONS_H
