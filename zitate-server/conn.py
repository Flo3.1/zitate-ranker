#!/usr/bin/python3.8


#required
#sudo apt install python3-psycopg2
import math
import psycopg2
import datetime

constring="host='localhost' port='12346' dbname='protserver' user='postgres' password='pass123'"
tablename="quotes"
reportname="reports"


class pgconn():
	def __init__(self):
		self.connection=psycopg2.connect(constring)
		cursor=self.connection.cursor()
		cursor.execute("SELECT tablename FROM pg_catalog.pg_tables WHERE tablename = %s", (tablename,))
		
		response=cursor.fetchone()
		if response==None:
			cursor.execute("CREATE TABLE "+ tablename +" (id INTEGER PRIMARY KEY, title VARCHAR, datum DATE,  quote JSON, points float(16), votes INTEGER, reports INTEGER, ishidden BOOLEAN);")
			cursor.execute("INSERT INTO "+ tablename +" VALUES( -1, 'Test', '1970-01-01', NULL, -1, -1, -1, true)")
			self.connection.commit()
			
			
			
		cursor.execute("SELECT tablename FROM pg_catalog.pg_tables WHERE tablename = %s", (reportname,))
		
		response=cursor.fetchone()
		if response==None:
			cursor.execute("CREATE TABLE "+ reportname +" (reportid INTEGER PRIMARY KEY, quoteid INTEGER, content VARCHAR, person VARCHAR, isresolver BOOLEAN );")
			cursor.execute("INSERT INTO "+ reportname +" VALUES( -1,-1, 'Report ist raus', 'Der administrator', true)")
			self.connection.commit()


	def get_random_quotes(self):
		
		
		cursor=self.connection.cursor()
		cursor.execute("""
			SELECT id FROM """+tablename+"""
			WHERE ishidden = 'f'
			ORDER BY votes, RANDOM() ASC
			LIMIT 2
			""")
		ids={}
		ids["0"]=response=cursor.fetchone()[0]
		
		ids["1"]=response=cursor.fetchone()[0]
		
		return ids
		

	def get_sorted(self, key, up, startval, lenght):
		
		
		if not(key in ["id", "title", "points", "datum", "RANDOM()", "reports"] and up in ["ASC", "DESC"] and type(startval) is int and type(lenght) is int):
			return -1
		
	
		
		cursor=self.connection.cursor()
		cursor.execute("""
			SELECT id, title, points, datum FROM """+tablename+"""
			WHERE ishidden = 'f'
			ORDER BY """+key+""" """+up+""", id ASC
			LIMIT """+str(lenght)+" OFFSET "+str(startval))
		
		retvalue={}
		k=0
		for i in cursor:
			retvalue[k]={}
			retvalue[k]["id"]=i[0]
			retvalue[k]["title"]=i[1]
			retvalue[k]["points"]=i[2]
			retvalue[k]["date"]=i[3].strftime('%Y-%m-%d')
			k+=1
		retvalue["lenght"]=k;
		return retvalue
		
		
	def report_quote(self, quote_id, report_text, report_author):
		
		
		cursor=self.connection.cursor()
		
		cursor.execute("SELECT id FROM "+ tablename +" WHERE id=%s", (int(quote_id),))
		res=cursor.fetchone();
		if res==None:
			return -1
		
		cursor.execute("UPDATE "+ tablename +" SET reports=reports+1 WHERE id=%s", (int(quote_id),))
		
		
		cursor.execute("INSERT INTO "+ reportname +" VALUES( (SELECT reportid FROM "+ reportname +" ORDER BY reportid DESC LIMIT 1)+1, %s, %s, %s, false)", (int(quote_id), report_text, report_author))
		self.connection.commit()
		
		


	def get_quote_by_id(self, quote_id):
		#print(f"Getting quote {quote_id}")

		cursor=self.connection.cursor()
		cursor.execute("SELECT title, quote, points, votes, datum, ishidden FROM "+ tablename +" WHERE id = %s AND ishidden='f'", (quote_id,))
		response=cursor.fetchone()

		if response==None:
			return -1
		else:
			retvalue={}
			retvalue["id"]=quote_id
			retvalue["title"]=response[0]
			retvalue["quote"]=response[1]
			retvalue["points"]=response[2]
			retvalue["votes"]=response[3]
			retvalue["date"]=response[4].strftime('%Y-%m-%d')
			retvalue["ishidden"]=response[5]
		return retvalue

	def get_quote_by_id_hidden(self, quote_id):

		cursor=self.connection.cursor()
		cursor.execute("SELECT title, quote, points, votes, datum, ishidden FROM "+ tablename +" WHERE id = %s", (quote_id,))
		response=cursor.fetchone()

		if response==None:
			return -1
		else:
			retvalue={}
			retvalue["id"]=quote_id
			retvalue["title"]=response[0]
			retvalue["quote"]=response[1]
			retvalue["points"]=response[2]
			retvalue["votes"]=response[3]
			retvalue["date"]=response[4].strftime('%Y-%m-%d')
			retvalue["ishidden"]=response[5]
		return retvalue

	def get_quote_by_title_hidden(self, title):

		cursor=self.connection.cursor()
		cursor.execute("SELECT id, quote, points, votes, datum, ishidden FROM "+ tablename +" WHERE title = %s", (title,))
		response=cursor.fetchone()

		if response==None:
			return -1
		else:
			retvalue={}
			retvalue["id"]=response[0]
			retvalue["title"]=title
			retvalue["quote"]=response[1]
			retvalue["points"]=response[2]
			retvalue["votes"]=response[3]
			retvalue["date"]=response[4].strftime('%Y-%m-%d')
			retvalue["ishidden"]=response[5]
		return retvalue


	#some system like ELO would be nice
	#there is the used system outlined https://en.wikipedia.org/wiki/Elo_rating_system#Mathematical_details
	#
	def rate_quotes(self, better_id, worse_id):
		print("rating quotes")
		
		cursor=self.connection.cursor()
		cursor.execute("SELECT points FROM "+ tablename +" WHERE id = %s AND ishidden= 'f'", (better_id,))
		
		
		#get first result
		response=cursor.fetchone()
		if response==None:
			return -1
		better_rating=response[0];
		
		
		#just in case there are three results: Panic
		response=cursor.fetchone()
		if response!=None:
			return -2
		
		cursor.execute("SELECT points FROM "+ tablename +" WHERE id = %s AND ishidden= 'f'", (worse_id,))
		#get second result
		response=cursor.fetchone()
		if response==None:
			return -1
		worse_rating=response[0];
		
		
		#just in case there are three results: Panic
		response=cursor.fetchone()
		if response!=None:
			return -2
		
		
		expectet_better=1/( 1+pow(10, ((worse_rating-better_rating)/400)) )
		expectet_worse=1/( 1+pow(10, ((better_rating-worse_rating)/400)) )
		
		better_rating+=124*(1-expectet_better)
		worse_rating+=124*(0-expectet_worse)
		
		cursor.execute("UPDATE "+ tablename +" SET points = %s, votes = votes +1 WHERE id = %s ", (better_rating, better_id))
		cursor.execute("UPDATE "+ tablename +" SET points = %s, votes = votes +1 WHERE id = %s ", (worse_rating, worse_id))
		self.connection.commit()
		return 0
		
		
		
		
		
	#if the user cant decide
	def rate_equal(self, better_id, worse_id):
		print("rating quotes")
		
		cursor=self.connection.cursor()
		cursor.execute("SELECT points FROM "+ tablename +" WHERE id = %s AND ishidden= 'f'", (better_id,))
		
		
		#get first result
		response=cursor.fetchone()
		if response==None:
			return -1
		better_rating=response[0];
		
		
		#just in case there are three results: Panic
		response=cursor.fetchone()
		if response!=None:
			return -2
		
		cursor.execute("SELECT points FROM "+ tablename +" WHERE id = %s AND ishidden= 'f'", (worse_id,))
		#get second result
		response=cursor.fetchone()
		if response==None:
			return -1
		worse_rating=response[0];
		
		
		#just in case there are three results: Panic
		response=cursor.fetchone()
		if response!=None:
			return -2
		
		
		expectet_better=1/( 1+pow(10, ((worse_rating-better_rating)/400)) )
		expectet_worse=1/( 1+pow(10, ((better_rating-worse_rating)/400)) )
		
		better_rating+=32*(0.5-expectet_better)
		worse_rating+=32*(0.5-expectet_worse)
		
		cursor.execute("UPDATE "+ tablename +" SET points = %s, votes = votes +1 WHERE id = %s ", (better_rating, better_id))
		cursor.execute("UPDATE "+ tablename +" SET points = %s, votes = votes +1 WHERE id = %s ", (worse_rating, worse_id))
		self.connection.commit()
		return 0
		
		
	def add_quote(self, quote, title, date):
		
		
		cursor=self.connection.cursor()
		
		#adds one to the higest is
		cursor.execute("INSERT INTO "+ tablename +" VALUES( (SELECT id FROM "+ tablename +" ORDER BY id DESC LIMIT 1)+1, %s, %s, %s, 1000, 0, 0, false)", (title, date, quote))
		
		
		self.connection.commit()
		
		
	def resolve_report(self, reportID):
		
		cursor=self.connection.cursor()
		
		#adds one to the higest is
		cursor.execute("UPDATE "+ reportname +" SET isresolver=true WHERE reportid=%s", (reportID,))
		cursor.execute("UPDATE "+ tablename +" SET reports=reports-1 WHERE id=(SELECT quoteid FROM "+reportname+" WHERE reportid=%s)", (reportID,))
		
		
		self.connection.commit()
		
		
	def update_quote(self,quoteid, quote, title, date):
		
		
		cursor=self.connection.cursor()
		
		#adds one to the higest is
		cursor.execute("UPDATE "+ tablename +" SET title=%s, datum=%s, quote=%s WHERE id= %s", (title,date,quote,quoteid))
		
		
		self.connection.commit()
		
	def toggle_hide(self,quoteid):
		
		
		cursor=self.connection.cursor()
		
		#adds one to the higest is
		cursor.execute("UPDATE "+ tablename +" SET ishidden=NOT ishidden WHERE id= %s", (quoteid,))
		
		
		self.connection.commit()
		
	def dump_active_reports(self):
		
		cursor=self.connection.cursor()
		
		cursor.execute("""
			SELECT reportid, quoteid, person, content FROM """+reportname+"""
			WHERE isresolver = 'f'
			ORDER BY reportid ASC
			""")
		
		retvalue={}
		k=0
		for i in cursor:
			retvalue[k]={}
			retvalue[k]["rid"]=i[0]
			retvalue[k]["qid"]=i[1]
			retvalue[k]["author"]=i[2]
			retvalue[k]["content"]=i[3]
			k+=1
		retvalue["lenght"]=k;
		
		self.connection.commit()
		return retvalue
		
	def __del__(self):
		if self.connection:
			self.connection.close()

	
