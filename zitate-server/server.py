#!/usr/bin/python3.8

import sys
import json
from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer
from conn import pgconn 

class requestHandler(BaseHTTPRequestHandler):
	
	
	
	
	def send_error_404(self):
		self.send_header('Content-type','text/html')
		self.send_error(404, 'not found')
		self.end_headers()
	
	def send_error_400(self):
		self.send_header('Content-type','text/html')
		self.send_error(400, 'bad request')
		self.end_headers()
		
		
	def send_error_500(self):
		self.send_header('Content-type','text/html')
		self.send_error(500, 'internal server error')
		self.end_headers()


	def do_POST(self):
		paths=self.path.split('/')
		if len(paths)>=2 and paths[1]=='api':
			if len(paths)>=3 and paths[2]=='quote':
							
				if len(paths)>=6 and paths[3]=='rate':
					
					db=pgconn()
					rcode=db.rate_quotes(int(paths[4]), int(paths[5]));
					
					if rcode==-1:
						self.send_error_400()
					elif rcode==-2:
						self.send_error_500()
					
					self.send_response(200)
					self.end_headers()
					del db
				elif len(paths)>=6 and paths[3]=='equal':
					
					db=pgconn()
					rcode=db.rate_equal(int(paths[4]), int(paths[5]));
					
					if rcode==-1:
						self.send_error_400()
					elif rcode==-2:
						self.send_error_500()
					
					self.send_response(200)
					self.end_headers()
					del db
					
				elif len(paths)>=4 and paths[3]=='report':
					
					db=pgconn()
					#print(self.rfile.read().decode("utf-8"))
					length = int(self.headers['content-length'])
					inputdata=json.loads(self.rfile.read(length).decode("utf-8"))
					
					
					if not("name" in inputdata and "message" in inputdata):
						self.send_error_400()
					
					
					rcode=db.report_quote(int(paths[4]), inputdata["message"], inputdata["name"])
					if rcode==-1:
						self.send_error_404()
					
					
					self.send_response(200)
					self.end_headers()
					del db
					
				else:
					
					self.send_error_404()

			else:
				self.send_error_404()
		else:
			
			self.send_error_404()

	def do_GET(self):
		paths=self.path.split('/')
		if len(paths)>=2 and paths[1]=='api':
			if len(paths)>=3 and paths[2]=='quote':
				if len(paths)>=4 and paths[3]=='rndpair':
					
					
					
					db=pgconn()
					message=json.dumps(db.get_random_quotes())
					
					
					self.send_response(200)
					self.send_header('Content-type','application/json')
					self.end_headers()
					self.wfile.write(bytes(message, "utf8"))
					del db
					
				elif len(paths)>=5 and paths[3]=='byid':
					
					db=pgconn()
					quote=db.get_quote_by_id(int(paths[4]));
					
					if quote==-1:
						self.send_error_404()
					
					message=json.dumps(quote)
					
					self.send_response(200)
					self.send_header('Content-type','application/json')
					self.end_headers()
					self.wfile.write(bytes(message, "utf8"))
					del db
					
					
				elif len(paths)>=7 and paths[3]=='list':
					
					db=pgconn()
					
					
					lists=db.get_sorted(paths[4], paths[5], int(paths[6])*20, 20)
					if lists==-1:
						self.send_error_400()
					
					message=json.dumps(lists)
					self.send_response(200)
					self.send_header('Content-type','application/json')
					self.end_headers()
					self.wfile.write(bytes(message, "utf8"))
					del db
					
				else:
					
					self.send_error_404()

			else:
				self.send_error_404()
		else:
			
			self.send_error_404()


if __name__=='__main__':
	

	server=ThreadingHTTPServer(('', 8000), requestHandler)
	server.serve_forever()

