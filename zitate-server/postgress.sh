#!/bin/bash

sudo docker run --rm -i --name=postgres-conn -p 12346:5432 -v $(pwd)/postgres-volume:/var/lib/postgresql/data -e POSTGRES_DB=protserver -e POSTGRES_PASSWORD=pass123 postgres
